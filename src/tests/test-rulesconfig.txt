check_freq=60

cgroup_load=true
type_load=true
rule_load=true

apply_nice=true
apply_latnice=true
apply_ioclass=true
apply_ionice=true
apply_sched=true
apply_oom_score_adj=true
apply_cgroup=true

cgroup_realtime_workaround=false
log_applied_rule=false
loglevel=info
